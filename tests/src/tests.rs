use super::*;
use ckb_testtool::context::Context;
use ckb_testtool::ckb_types::{
    core::TransactionBuilder,
    core::TransactionView,
    bytes::Bytes,
    packed::*,
    prelude::*,
};

use auction_utils;
use auction_utils::{
    types,
    types::{
        AuctionState,
        Request,
    },
};
use rebase_auction::{self, rebase, AvoumKey};
use std::collections::btree_map::BTreeMap;
use ckb_sodium::*;

const MAX_CYCLES: u64 = 100_000_000;

const AUCTION_STATE_CAPACITY: u64 = 500u64; // somewhat arbitrary; TODO: fit to data

/// Helper to deploy one of the scripts in `../contracts` by name.
fn deploy_script(context: &mut Context, name: &str) -> OutPoint {
    let bin = Loader::default().load_binary(name);
    context.deploy_cell(bin)
}

/// XXX DIRTY AWFUL HACK
///
/// auction_utils defines From<packed::_> for a couple of its types, but it
/// defines these on the types in ckb_standalone_types, while the types exposed
/// by ckb_testtool are those of ckb_types.
///
/// From discussion on discord, I have been given the impression that this is
/// due to a coordination issue between separate teams.
///
/// However, auction_utils *cannot* use the ckb_types crate, since it is imported
/// from ckb vm code and therefore *must* be no_std.
///
/// Both types from both crates are the same, and are in fact newtype wrappers around
/// bytes. So, for now, we just add a helper functions that do unsafe casts where
/// we need them, using mem::transmute under the hood.
///
/// Ideally, ckb_types would depend on ckb_standalone_types, rather than generating its
/// own types from the same molecule schema.
mod cast {
    use ckb_standalone_types::packed as standalone;
    use ckb_testtool::ckb_types::packed as test;

    pub fn script(s: test::Script) -> standalone::Script {
        unsafe {
            core::mem::transmute(s)
        }
    }

    // Same thing, but other direction.
    pub fn from_script(s: standalone::Script) -> test::Script {
        unsafe {
            core::mem::transmute(s)
        }
    }

    pub fn out_point(o: test::OutPoint) -> standalone::OutPoint {
        unsafe {
            core::mem::transmute(o)
        }
    }

    pub fn transaction(tx: test::Transaction) -> standalone::Transaction {
        unsafe {
            core::mem::transmute(tx)
        }
    }

    pub fn from_transaction(tx: standalone::Transaction) -> test::Transaction {
        unsafe {
            core::mem::transmute(tx)
        }
    }
}

/// Various auction-global information.
struct Env {
    context: Context,
    script_outs: ScriptOutPoints,
    scripts: Scripts,
    auction_id: AvoumKey,
}

/// A bundle of cells containing the various code of scripts of interest.
struct ScriptOutPoints {
    typ: OutPoint,
    escrow_lock: OutPoint,
    bid_lock: OutPoint,
    sig_lock: OutPoint,
    noop_lock: OutPoint,
}

impl ScriptOutPoints {
    fn new(mut context: &mut Context) -> Self {
        ScriptOutPoints {
            typ: deploy_script(&mut context, "avoum-auction-type"),
            escrow_lock: deploy_script(&mut context, "avoum-auction-escrow-lock"),
            bid_lock: deploy_script(&mut context, "avoum-auction-bid-lock"),
            sig_lock: deploy_script(&mut context, "avoum-auction-sig-lock"),
            noop_lock: deploy_script(&mut context, "avoum-noop-lock"),
        }
    }

    /// Add all of the scripts to the transaction's cell_deps
    fn add_to_tx_deps(&self, mut tx: TransactionBuilder) -> TransactionBuilder {
        let outs = [
            &self.typ,
            &self.escrow_lock,
            &self.bid_lock,
            &self.sig_lock,
            &self.noop_lock,
        ];
        for out in outs.iter() {
            tx = tx.cell_dep(CellDep::new_builder()
                             .out_point((*out).clone())
                             .build());
        }
        tx
    }
}

/// Like `ScriptOutPoints`, but for full scripts, not just the code.
/// Note that this is missing `bid_lock`; since we use that with
/// differing arguments, a single cached `Script` structure is not
/// useful.
struct Scripts {
    typ: Script,
    escrow_lock: Script,
    seller_lock: Script,
    noop_lock: Script,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
/// Parameters used on auction creation.
struct AuctionCfg {
    deadline_block: u64,
}

/// Helper to build a transaction that closes an auction. `prev_tx` should
/// be the last transaction that touched the auction, while `cfg` should be
/// the config that was passed to `open_auction` when the auction was created.
fn close_auction(env: &mut Env, prev_tx: Transaction, cfg: &AuctionCfg) -> TransactionView {
    //let auction_in = prev_tx.raw().outputs().get(0).unwrap();
    let assets_in = prev_tx.raw().outputs().get(1).unwrap();
    let bid_in = prev_tx.raw().outputs().get(2).unwrap();

    let assets_data = prev_tx.raw().outputs_data().get(1).unwrap();
    let bid_data = prev_tx.raw().outputs_data().get(2).unwrap();

    let prev_hash = prev_tx.calc_tx_hash();

    let mk_input = |i| {
        CellInput::new_builder()
            .previous_output(OutPoint::new(prev_hash.clone(), i))
            .build()
    };

    let inputs = CellInputVec::new_builder()
        .push(mk_input(0))
        .push(mk_input(1))
        .push(mk_input(2))
        .build();

    let bid_out = CellOutput::new_builder()
        .capacity(bid_in.capacity())
        .type_(bid_in.type_())
        .lock(env.scripts.seller_lock.clone())
        .build();

    // Get the lock script we should use to send out the assets.
    let refund_lock_script = {
        let req: Request = auction_utils::decode_slice(
            &prev_tx.witnesses().get(0).unwrap().as_reader().raw_data()[..],
        ).unwrap();
        match req {
            Request::Bid { refund_lock_script, .. } => refund_lock_script,
            _ => {
                // TODO: possible enhancement: allow constructing close
                // transactions where previous request was an Open; this
                // is legal.
                panic!("Can't close after a non-bid.")
            }
        }
    };

    let assets_out = CellOutput::new_builder()
        .capacity(assets_in.capacity())
        .type_(assets_in.type_())
        .lock(cast::from_script(refund_lock_script.into()))
        .build();

    // Build a minimal header as a proof that the deadline has passed.
    // XXX: this is fragile; we're ignoring a lot of fields, and relying
    // on ckb_testtool to not check things too thoroughly. This is fine
    // for now; it's orthogonal to what we're trying to test, but this
    // test could concievably break if testtool starts looking at other
    // data.
    //
    // We do it this way mostly for expidiency; esp. given that the
    // header includes the hash of the previous block, we'd have to
    // construct an entire chain by hand in order to this "right,"
    // and that doesn't seem worth the effort.
    let header = Header::new_builder()
        .raw(RawHeader::new_builder()
             .number((cfg.deadline_block + 5).pack())
             .build())
        .build()
        .into_view();

    let header_deps = Byte32Vec::new_builder()
        .push(header.hash())
        .build();

    env.context.headers.insert(header.hash(), header);

    let outputs = CellOutputVec::new_builder()
        .push(bid_out)
        .push(assets_out)
        .build();

    let outputs_data = BytesVec::new_builder()
        .push(bid_data)
        .push(assets_data)
        .build();

    let mut tx = TransactionBuilder::default()
        .inputs(inputs.clone())
        .outputs(outputs)
        .outputs_data(outputs_data)
        .header_deps(header_deps)
        .witness(Bytes::from(auction_utils::encode_vec(&Request::Close).unwrap()).pack());

    // TODO(cleanup): factor out this copypasta from place_bid.
    for input in inputs {
        tx = tx.cell_dep(CellDep::new_builder()
                         .out_point(input.previous_output())
                         .build());
    }
    env.script_outs.add_to_tx_deps(tx).build()
}

/// Build a transaction that places a new bid in the auction. The bid & refund lock
/// scripts will be based on `keypair`.
fn place_bid(env: &mut Env, keypair: &KeyPair, prev_tx: Transaction, amount: u64) -> TransactionView {
    let raw_tx = prev_tx.raw();

    let prev_auction_state = raw_tx
        .outputs_data()
        .get(0)
        .expect("No outputs in prev tx.");
    let prev_auction_state: types::AuctionState =
        auction_utils::decode_slice(&prev_auction_state
                                    .as_reader().raw_data()[..])
        .expect("Decoding previous auction state.");

    let refund_lock_script = types::Script::from(cast::script(
        env.context
        .build_script(&env.script_outs.sig_lock,
                      Bytes::from(Vec::from(&keypair.pk.0[..])))
        .expect("lock script")
    ));
    let request = auction_utils::encode_vec(&Request::Bid {
        refund_lock_script: refund_lock_script.clone(),
        retract_key: keypair.pk,
    }).expect("encoding request");

    let bid_lock_data = auction_utils::encode_vec(&types::bid_lock::Data {
        request_hash: types::Hash::from_data(&request[..]),
        auction_script: types::Script::from(cast::script(env.scripts.typ.clone())),
        avoum_id: prev_auction_state.avoum_id,
        pub_key: keypair.pk,
    }).expect("Encoding bid_lock::Data");

    let bid_lock_script = env.context
        .build_script(&env.script_outs.bid_lock,
                      Bytes::from(Vec::from(types::Hash::from_data(&bid_lock_data[..])
                                            .digest)))
        .expect("bid lock script");

    let new_bid_data = Bytes::new();
    let new_bid_cell = env.context.create_cell(
        CellOutput::new_builder()
            .capacity(amount.pack())
            .lock(bid_lock_script)
            .build(),
        new_bid_data.clone(),
    );

    let auction_in: types::AuctionState = auction_utils::decode_slice(
        raw_tx
        .outputs_data().get(0).expect("auction state")
        .as_reader()
        .raw_data()
    ).expect("Decoding auction state");

    let mut auction_out = auction_in.clone();
    auction_out.current_bid = amount;
    auction_out.refund_lock_script = refund_lock_script;

    let type_script_opt =
        ScriptOptBuilder::default()
            .set(Some(env.scripts.typ.clone()))
            .build();

    let mut inputs: Vec<_> = (0..2).map(|i| {
        CellInput::new_builder()
            .previous_output(env.context.create_cell(
                raw_tx.outputs().get(i).unwrap(),
                raw_tx.outputs_data().get(i).unwrap().raw_data(),
            ))
            .build()
    }).collect();
    inputs.push(
        CellInput::new_builder()
            .previous_output(new_bid_cell.clone())
            .build()
    );

    let old_bid = raw_tx.outputs().get(2);
    let old_bid_data = raw_tx.outputs_data().get(2);

    // Append old bid, if any:
    if let Some(old_bid) = old_bid.clone() {
        inputs.push(CellInput::new_builder()
            .previous_output(env.context.create_cell(
                old_bid,
                old_bid_data.clone().unwrap().raw_data(),
            ))
            .build())
    }

    let mut outputs = vec![
        CellOutput::new_builder()
            .capacity(AUCTION_STATE_CAPACITY.pack())
            .type_(type_script_opt)
            .lock(env.scripts.noop_lock.clone())
            .build(),
        raw_tx.outputs().get(1).unwrap(),
        CellOutput::new_builder()
            .capacity(amount.pack())
            .lock(env.scripts.escrow_lock.clone())
            .build()
    ];
    if let Some(old_bid) = old_bid.clone() {
        outputs.push(CellOutput::new_builder()
                     .capacity(old_bid.capacity())
                     .lock(cast::from_script(auction_in.refund_lock_script.clone().into()))
                     .build())
    }

    let mut outputs_data = vec![
        Bytes::from(auction_utils::encode_vec(&auction_out)
                    .expect("encoding output")).pack(),
        raw_tx.outputs_data().get(1).expect("No ouptut"),
        new_bid_data.pack(),
    ];
    if let Some(old_bid_data) = old_bid_data.clone() {
        outputs_data.push(old_bid_data
                          .raw_data().pack());
    }

    let mut tx = TransactionBuilder::default()
        .inputs(inputs.clone())
        .outputs(outputs)
        .outputs_data(outputs_data)
        .witness(Bytes::from(request).pack())
        .witness(Bytes::from(bid_lock_data).pack());
    for input in inputs {
        tx = tx.cell_dep(CellDep::new_builder()
                         .out_point(input.previous_output())
                         .build());
    }
    env.script_outs.add_to_tx_deps(tx).build()
}

/// Open a new auction, given the sellers keypair and a config.
fn open_auction(seller_keypair: &KeyPair, cfg: &AuctionCfg) -> (TransactionView, Env) {
    let mut context = Context::default();

    // prepare scripts
    let script_outs = ScriptOutPoints::new(&mut context);
    let type_script = context
        .build_script(&script_outs.typ, Bytes::new()) // Args are currently unused.
        .expect("type script");
    let type_script_opt =
        ScriptOptBuilder::default()
            .set(Some(type_script.clone()))
            .build();
    let noop_lock_script = context
        .build_script(&script_outs.noop_lock, Bytes::new())
        .expect("noop lock script");
    let seller_lock_script = context
        .build_script(&script_outs.sig_lock,
                      Bytes::from(Vec::from(&seller_keypair.pk.0[..])))
        .expect("test lock script");


    // Asset being sold. Just CKB currently, though we could use any cell whatsoever;
    // the contract just ensures that its (type, capacity, contents) is unchanged from
    // one tx to the next.
    //
    // TODO: factoring this out into a parameter might improve clarity, even if we
    // never actually try it with something else.
    let assets_ckb = 1000u64;
    let assets_data = Bytes::new();
    let assets_input_out_point = context.create_cell(
        CellOutput::new_builder()
            .capacity(assets_ckb.pack())
            .lock(seller_lock_script.clone())
            .build(),
        assets_data.clone(),
    );

    let assets_input = CellInput::new_builder()
        .previous_output(assets_input_out_point.clone())
        .build();

    let seller_lock_script_parsed: types::Script = cast::script(seller_lock_script.clone()).into();

    let avoum_id = types::AvoumId::new(&cast::out_point(assets_input_out_point), 0);

    let escrow_lock_script = context
        .build_script(&script_outs.escrow_lock,
                      Bytes::from(Vec::from(avoum_id
                                            .hash_with_type(&cast::script(type_script.clone()))
                                            .digest)))
        .expect("escrow lock script");

    let initial_state = AuctionState {
        avoum_id,
        current_bid: 0,
        deadline_block: cfg.deadline_block,

        escrow_lock_script: cast::script(escrow_lock_script.clone()).into(),

        // We set these as initially equal, so if no-one bids, the seller
        // can get their assets back:
        seller_lock_script: seller_lock_script_parsed.clone(),
        refund_lock_script: seller_lock_script_parsed,
    };

    let auction_contents_bytes = Bytes::from(auction_utils::encode_vec(&initial_state)
                                             .expect("encoding initial state"));

    let outputs = vec![
        // Auction state:
        CellOutput::new_builder()
            .capacity(AUCTION_STATE_CAPACITY.pack()) // somewhat arbitrary; TODO: fit to data.
            .type_(type_script_opt.clone())
            .lock(noop_lock_script.clone())
            .build(),
        // Assets being sold:
        CellOutput::new_builder()
            .capacity(assets_ckb.pack())
            .lock(escrow_lock_script.clone())
            .build(),
    ];

    let outputs_data = vec![
        auction_contents_bytes,
        assets_data.clone(),
    ];

    // build transaction
    let tx = TransactionBuilder::default()
        .input(assets_input)
        .outputs(outputs)
        .outputs_data(outputs_data.pack())
        .witness(Bytes::from(auction_utils::encode_vec(&Request::Open)
                             .expect("encoding request"))
                 .pack())
        .build();
    let tx = context.complete_tx(tx);
    let tx_hash = tx.hash();
    let sig = sign_detached(tx_hash.as_reader().raw_data(), &seller_keypair.sk);
    let tx = tx
        .as_advanced_builder()
        .witness(Bytes::from(Vec::from(sig.to_bytes())).pack())
        .build();

    let scripts = Scripts {
        typ: type_script.clone(),
        escrow_lock: escrow_lock_script,
        seller_lock: seller_lock_script,
        noop_lock: noop_lock_script,
    };
    let env = Env {
        context,
        script_outs,
        scripts,
        auction_id: AvoumKey {
            identity: avoum_id.unique_hash.digest,
            type_script: cast::script(type_script).into(),
        },
    };
    (tx, env)
}

/// Add all of the outputs from tx to ctx.cells, so that future transactions
/// can find them.
fn register_outputs(ctx: &mut Context, tx: &TransactionView) {
    let tx_hash = tx.hash();
    let tx = tx.data();
    for i in 0..tx.raw().outputs().len() {
        ctx.cells.insert(
            OutPoint::new(tx_hash.clone(), i as u32),
            (
                tx.raw().outputs().get(i).unwrap(),
                tx.raw().outputs_data().get(i).unwrap().raw_data(),
            ),
        );
    }
}

#[test]
/// General test of the usual auction flow.
fn test_sequence() {
    let seller_keypair = gen_keypair();
    let bidder_1_keypair = gen_keypair();
    let bidder_2_keypair = gen_keypair();

    let cfg = AuctionCfg {
        deadline_block: 200, // Arbitrary.
    };

    // Opening the tx should succeed.
    let (tx, mut env) = open_auction(&seller_keypair, &cfg);
    assert!(env.context.verify_tx(&tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &tx);

    // Placing an initial bid of 5 should succeed
    let tx = place_bid(&mut env, &bidder_1_keypair, tx.data(), 5);
    assert!(env.context.verify_tx(&tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &tx);

    // Placing a larger bid should succeed
    let tx = place_bid(&mut env, &bidder_2_keypair, tx.data(), 10);
    assert!(env.context.verify_tx(&tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &tx);

    // Placing a smaller bid should fail
    let bad_tx = place_bid(&mut env, &bidder_1_keypair, tx.data(), 7);
    assert!(env.context.verify_tx(&bad_tx, MAX_CYCLES).is_err());

    // Rebasing a bid on top of a smaller bid should succeed:
    let orig_tx = place_bid(&mut env, &bidder_1_keypair, tx.data(), 15);
    let intervening_tx = place_bid(&mut env, &bidder_2_keypair, tx.data(), 12);
    assert!(env.context.verify_tx(&intervening_tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &intervening_tx);
    let mut latest_states = BTreeMap::new();
    let _ = latest_states.insert(
        env.auction_id.clone(),
        cast::transaction(intervening_tx.data()),
    );
    let tx = rebase(cast::transaction(orig_tx.data()), latest_states)
        .expect("Rebase failed");
    let tx = cast::from_transaction(tx).into_view();
    assert!(env.context.verify_tx(&tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &tx);

    // For good measure, add one more bid after the rebase:
    let tx = place_bid(&mut env, &bidder_2_keypair, tx.data(), 20);
    assert!(env.context.verify_tx(&tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &tx);

    // Try to close the auction:
    let tx = close_auction(&mut env, tx.data(), &cfg);
    assert!(env.context.verify_tx(&tx, MAX_CYCLES).is_ok());

    // TODO: test:
    //
    // - Various entirely-bogus txs, like bids that are malformed in
    //   one way or another.
}

#[test]
/// Test rebasing a Request::Close transction.
fn test_rebase_close() {
    let seller_keypair = gen_keypair();
    let bidder_1_keypair = gen_keypair();
    let bidder_2_keypair = gen_keypair();

    let cfg = AuctionCfg {
        deadline_block: 200, // Arbitrary.
    };

    // Open an auction, place an initial bid:
    let (tx, mut env) = open_auction(&seller_keypair, &cfg);
    assert!(env.context.verify_tx(&tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &tx);
    let tx = place_bid(&mut env, &bidder_1_keypair, tx.data(), 5);
    assert!(env.context.verify_tx(&tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &tx);

    // Build a tx that closes the auction:
    let orig_tx = close_auction(&mut env, tx.data(), &cfg);

    // Place an intervening bid:
    let intervening_tx = place_bid(&mut env, &bidder_2_keypair, tx.data(), 10);
    assert!(env.context.verify_tx(&intervening_tx, MAX_CYCLES).is_ok());
    register_outputs(&mut env.context, &intervening_tx);

    // Rebase the closing transaction and apply it:
    let mut latest_states = BTreeMap::new();
    let _ = latest_states.insert(
        env.auction_id.clone(),
        cast::transaction(intervening_tx.data()),
    );
    let tx = rebase(cast::transaction(orig_tx.data()), latest_states)
        .expect("Rebase failed");
    let tx = cast::from_transaction(tx).into_view();
    env.context.verify_tx(&tx, MAX_CYCLES).unwrap();
}
