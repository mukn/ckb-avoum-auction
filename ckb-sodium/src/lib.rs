#![no_std]

use serde::{Serialize, Deserialize};

extern {
    fn crypto_sign_detached(sig: *mut u8,
                            siglen_p: *mut usize,
                            m: *const u8,
                            mlen: usize,
                            sk: *const u8) -> i32;

    fn crypto_sign_verify_detached(sig: *const u8,
                                   m: *const u8,
                                   mlen: usize,
                                   pk: *const u8) -> i32;

    fn crypto_sign_keypair(pk: *mut u8, sk: *mut u8) -> i32;
}

pub const PUBLICKEYBYTES: usize = 32;
pub const SECRETKEYBYTES: usize = 64;
pub const SIGBYTES: usize = 64;

#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Eq)]
pub struct PublicKey(pub [u8; PUBLICKEYBYTES]);

#[derive(Debug, Copy, Clone)]
pub struct SecretKey(pub [u8; SECRETKEYBYTES]);

#[derive(Debug, Copy, Clone)]
pub struct Signature([u8; SIGBYTES]);

#[derive(Debug, Copy, Clone)]
pub struct KeyPair {
    pub pk: PublicKey,
    pub sk: SecretKey,
}

impl Signature {
    pub fn new(bytes: [u8; SIGBYTES]) -> Self {
        Signature(bytes)
    }

    pub fn to_bytes(&self) -> [u8; SIGBYTES] {
        self.0
    }
}

impl From<[u8; 64]> for Signature {
    fn from(value: [u8; SIGBYTES]) -> Self {
        Signature(value)
    }
}

pub fn sign_detached(m: &[u8], sk: &SecretKey) -> Signature {
    let mut sigbuf: [u8; SIGBYTES] = [0; SIGBYTES];
    let mut siglen_p: usize = 0;
    unsafe {
        let ok = crypto_sign_detached(&mut sigbuf[0] as *mut u8,
                                      &mut siglen_p as *mut usize,
                                      m.as_ptr(),
                                      m.len(),
                                      &sk.0[0] as *const u8);
        assert_eq!(ok, 0);
        assert_eq!(siglen_p, SIGBYTES);
        Signature::from(sigbuf)
    }
}

pub fn verify_detached(sig: &Signature, m: &[u8], pk: &PublicKey) -> bool {
    unsafe {
        crypto_sign_verify_detached(&sig.to_bytes()[0] as *const u8,
                                    m.as_ptr(),
                                    m.len(),
                                    &pk.0[0] as *const u8) == 0
    }
}

pub fn gen_keypair() -> KeyPair {
    let mut pk: [u8; PUBLICKEYBYTES] = [0; PUBLICKEYBYTES];
    let mut sk: [u8; SECRETKEYBYTES] = [0; SECRETKEYBYTES];
    unsafe {
        assert_eq!(0, crypto_sign_keypair(&mut pk[0] as *mut u8,
                                          &mut sk[0] as *mut u8))
    }
    KeyPair {
        pk: PublicKey(pk),
        sk: SecretKey(sk),
    }
}
