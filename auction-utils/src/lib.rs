#![no_std]

use serde::{Serialize, Deserialize};
use serde_json;

extern crate alloc;

use alloc::vec::Vec;
pub mod types;

/// Decode a value from a slice. This currently uses json encoding, but the
/// wrapper function will let us swap in a different encoding easily if and
/// when we choose to.
pub fn decode_slice<'a, T>(v: &'a [u8]) -> serde_json::Result<T> where T: Deserialize<'a> {
    serde_json::from_slice(v)
}

/// Encode a vector. inverse of `decode_slice`.
pub fn encode_vec<T: ?Sized>(value: &T) -> serde_json::Result<Vec<u8>> where T: Serialize {
    Ok(serde_json::to_string(value)?.into_bytes())
}
