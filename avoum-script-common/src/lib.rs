#![no_std]

extern crate alloc;

use alloc::vec;
use alloc::vec::Vec;

use sha2::{Sha256, Digest};

use ckb_std::{
    high_level,
    syscalls,
    ckb_constants::Source,
    error::SysError,
};

use auction_utils;
use auction_utils::types;
use auction_utils::types::Request;

pub mod error;
pub mod sign;

/// Load the type script for the cell at the given slot
pub fn load_type_script(i: usize, s: Source) -> Option<types::Script> {
    high_level::load_cell_type(i, s)
        .unwrap()
        .map(types::Script::from)
}

/// Load the auction state stored in the given cell.
pub fn load_auction_state(index: usize, source: Source) -> Result<types::AuctionState, error::Error> {
    let bytes = high_level::load_cell_data(index, source)?;
    Ok(auction_utils::decode_slice(&bytes[..])?)
}

/// Load the request, as a `Vec<u8>`. This is always the first witness.
pub fn load_request_vec() -> Result<Vec<u8>, SysError> {
    load_witness_vec(0, Source::Input)
}

/// Load & deserialize the request witness.
pub fn load_request() -> Result<Request, error::Error> {
    let vec = load_request_vec()?;
    Ok(auction_utils::decode_slice(&vec[..])?)
}

/// Load the given witness, as a flat Vec<u8>.
pub fn load_witness_vec(index: usize, source: Source) -> Result<Vec<u8>, SysError> {
    let mut buf = vec![];
    match syscalls::load_witness(&mut buf[..], 0, index, source) {
        Ok(0) => Ok(buf),
        Ok(_) => {
            panic!("Should never happen: non-zero length returned on zero buffer.");
        },
        Err(SysError::LengthNotEnough(len)) => {
            buf.resize(len, 0);
            assert!(syscalls::load_witness(&mut buf[..], 0, index, source)? == len);
            Ok(buf)
        },
        Err(err) => Err(err),
    }
}

/// Loads an "associated" vector from the witnesses.
///
/// This is used by scripts that want to include a larger amount of data
/// in their args; they instead store a hash of that data and provide it
/// as a witness.
///
/// This function looks for a witness that contains data matching the hash
/// found in the current script's args, returning the first one it finds,
/// if any.
pub fn load_associated_vec() -> Result<Vec<u8>, SysError> {
    let args = high_level::load_script()?.args();
    for i in 0.. {
        let buf = load_witness_vec(i, Source::Input)?;
        let digest = Sha256::digest(&buf[..]);
        if args.as_reader().raw_data() == digest.as_slice() {
            return Ok(buf)
        }
    }
    // requires us to oveflow a usize before seeing any witnesses. We could
    // make an error type for this, but meh.
    panic!("impossible");
}
