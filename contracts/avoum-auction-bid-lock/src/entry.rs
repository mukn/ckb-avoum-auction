//! Lock script to be used for outstanding bids.
//!
//! This script permits transactions under one of two conditions:
//!
//! 1. Either the first input or the first output is the auction state for
//!    the auction we're supposed to be a part of.
//! 2. The transaction is signed by the bidder's public key.
//!
//! (1) is used when the bid actually takes part in the auction; we defer
//! most of the logic in that case to the type script.
//!
//! (2) is used for retracting bids; it lets the bidder consume the cell
//! without relying on the auction machinery.
//!
//! Note that the auciton type script doesn't care what the bid's lock
//! script is, so conceivably bidders could use some other lock script
//! at their descretion, so long as that script permits the bid to be
//! used in the auction. But this script is a good default.
use core::result::Result;
use avoum_script_common::{
    load_type_script,
    load_associated_vec,
    load_request_vec,
    load_auction_state,
    error::Error,
    sign::assert_tx_signed_by,
};
use ckb_std::{
    ckb_constants::Source,
};
use auction_utils::types;

/// Return whether the cell at `(i, source)` is the auction state for the
/// auction specified in `data`.
fn is_auction_cell(data: &types::bid_lock::Data, i: usize, source: Source) -> bool {
    let expected_script = Some(data.auction_script.clone()); // TODO(perf): avoid copy
    let actual_script = load_type_script(i, source);
    if actual_script != expected_script {
        return false;
    }

    let state = load_auction_state(i, source);
    match state {
        Err(_) => false,
        Ok(state) => {
            data.avoum_id == state.avoum_id
        }
    }
}

pub fn main() -> Result<(), Error> {
    let assoc = load_associated_vec()?;
    let assoc: types::bid_lock::Data = auction_utils::decode_slice(&assoc[..])?;
    let req = load_request_vec()?;

    assert!(types::Hash::from_data(&req[..]) == assoc.request_hash);

    if is_auction_cell(&assoc, 0, Source::Input) || is_auction_cell(&assoc, 0, Source::Output) {
        return Ok(());
    }

    // Not part of the auction. Maybe the bidder is trying to retract their bid; check for a
    // signature:
    if let types::Request::Bid { retract_key, .. } = auction_utils::decode_slice(&req[..])? {
        assert_tx_signed_by(&retract_key);
        Ok(())
    } else {
        panic!("Not part of an auction and no valid signature")
    }
}
