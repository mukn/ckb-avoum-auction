use core::result::Result;
use avoum_script_common::error::Error;
use ckb_std::{
    high_level,
    ckb_constants::Source,
};

use auction_utils;
use auction_utils::types;

pub fn main() -> Result<(), Error> {
    // Check if one of the cells in our transaction is the auction; if so,
    // allow, otherwise deny. The auction's type script takes over from there.

    let auction_bytes = high_level::load_cell_data(0, Source::Input)?;
    let auction_state: types::AuctionState = auction_utils::decode_slice(&auction_bytes[..])?;
    let auction_type = high_level::load_cell_type(0, Source::Input)?.expect("No type on auction cell");
    let args = high_level::load_script()?.args();
    let expected = args.as_reader().raw_data();
    let actual = &auction_state.avoum_id.hash_with_type(&auction_type).digest[..];

    assert!(expected == actual);

    Ok(())
}

