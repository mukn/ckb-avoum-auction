#![no_std]

// Enable some methods that won't be officially stabilized until Rust 2021
// edition:
#![feature(map_first_last)]

extern crate alloc;
mod tx_hash;

use alloc::collections::btree_map::BTreeMap;

use auction_utils::{
    self,
    types::{self, Request},
};
use ckb_standalone_types::{
    prelude::*,
    packed::{
        BytesVec,
        CellDep,
        CellDepVec,
        CellInput,
        CellInputVec,
        CellOutput,
        CellOutputVec,
        OutPoint,
        RawTransaction,
        Script,
        Transaction,
    },
};

#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord)]
pub struct AvoumKey {
    pub identity: [u8; 32],
    pub type_script: types::Script,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum RebaseError {
    /// `Obsolete` indicates that the state changed in such a way that the
    /// transaction is no longer applicable; rebasing it should be abandoned
    /// permanently.
    Obsolete,
}

/// This is the main entry point to the rebasing logic.
///
/// # Parameters
///
/// - `orig_tx`: The original transaction
/// - `latest_states`: A map of identites to the last transaction that
///    modified those identities.
///
/// # Returns
///
/// On success, returns a new transaction, which is semantically equivalent
/// to `orig_tx`, but applies to the current state. May return an error
/// if rebasing is unsuccessful.
pub fn rebase(orig_tx: Transaction, mut latest_states: BTreeMap<AvoumKey, Transaction>) -> Result<Transaction, RebaseError> {
    // This interface is designed to support the general case of AVOUM,
    // where there may be more than one cell with identity, only some of
    // which may be involved in a given transaction.
    //
    // ...but in the simple case we only actually have one cell with identity,
    // so just fish that out of the map and throw the rest away:
    let (_, last_tx) = latest_states.pop_first().expect("empty map");
    let orig_req = auction_utils::decode_slice(
        orig_tx.witnesses().get(0).unwrap().as_reader().raw_data(),
    ).unwrap();
    let last_req = auction_utils::decode_slice(
        last_tx.witnesses().get(0).unwrap().as_reader().raw_data(),
    ).unwrap();

    match last_req {
        Request::Close | Request::Open {..} => {
            // Can't rebase on top of Close, and Open doesn't
            // make any sense.
            return Err(RebaseError::Obsolete);
        }
        Request::Bid {..} => {},
    }

    let last_hash = tx_hash::calc_tx_hash(&last_tx);

    let prev_out_as_in = |i: u32| {
        CellInput::new_builder()
            .previous_output(
                OutPoint::new_builder()
                .tx_hash(last_hash.clone())
                .index(i.pack())
                .build()
            )
            .build()
    };

    match orig_req {
        Request::Bid {..} => {
            let auction_orig: types::AuctionState = auction_utils::decode_slice(
                orig_tx.raw().outputs_data().get(0).unwrap().as_reader().raw_data(),
            ).unwrap();
            let auction_last: types::AuctionState = auction_utils::decode_slice(
                last_tx.raw().outputs_data().get(0).unwrap().as_reader().raw_data(),
            ).unwrap();

            if auction_last.current_bid >= auction_orig.current_bid {
                // We have been out-bid; abandon the tx.
                return Err(RebaseError::Obsolete);
            }


            let inputs = CellInputVec::new_builder()
                .push(prev_out_as_in(0))
                .push(prev_out_as_in(1))
                .push(orig_tx.raw().inputs().get(2).unwrap())
                .push(CellInput::new_builder()
                      .previous_output(
                          OutPoint::new_builder()
                          .tx_hash(last_hash.clone())
                          .index(2u32.pack())
                          .build()
                      )
                      .build())
                .build();

            let outputs = CellOutputVec::new_builder()
                .push(last_tx.raw().outputs().get(0).unwrap())
                .push(last_tx.raw().outputs().get(1).unwrap())
                .push(orig_tx.raw().outputs().get(2).unwrap())
                .push(last_tx.raw().outputs().get(2).unwrap()
                    .as_builder()
                    .lock(Script::from(auction_last.refund_lock_script))
                    .build()
                )
                .build();
            let outputs_data = BytesVec::new_builder()
                .push(orig_tx.raw().outputs_data().get(0).unwrap())
                .push(last_tx.raw().outputs_data().get(1).unwrap())
                .push(orig_tx.raw().outputs_data().get(2).unwrap())
                .push(last_tx.raw().outputs_data().get(2).unwrap())
                .build();


            let deps = make_deps(orig_tx.raw().cell_deps(),
                                 last_tx.raw().cell_deps(),
                                 inputs.clone());

            let tx = Transaction::new_builder()
                .raw(RawTransaction::new_builder()
                     .inputs(inputs)
                     .outputs(outputs)
                     .outputs_data(outputs_data)
                     .cell_deps(deps)
                     .build())
                .witnesses(orig_tx.witnesses())
                .build();
            Ok(tx)
        },
        Request::Close => {
            let auction_in_data = last_tx.raw().outputs_data().get(0).unwrap();
            let auction_state: types::AuctionState = auction_utils::decode_slice(
                auction_in_data.as_reader().raw_data(),
            ).unwrap();


            let inputs = CellInputVec::new_builder()
                .push(prev_out_as_in(0))
                .push(prev_out_as_in(1))
                .push(prev_out_as_in(2))
                .build();

            let bid_in = last_tx.raw().outputs().get(2).unwrap();
            let bid_out = CellOutput::new_builder()
                .capacity(bid_in.capacity())
                .type_(bid_in.type_())
                .lock(auction_state.seller_lock_script.into())
                .build();
            let bid_data = last_tx.raw().outputs_data().get(2).unwrap();

            let assets_in = last_tx.raw().outputs().get(1).unwrap();
            let assets_out = CellOutput::new_builder()
                .capacity(assets_in.capacity())
                .type_(assets_in.type_())
                .lock(auction_state.refund_lock_script.into())
                .build();
            let assets_data = last_tx.raw().outputs_data().get(1).unwrap();

            let outputs = CellOutputVec::new_builder()
                .push(bid_out)
                .push(assets_out)
                .build();

            let outputs_data = BytesVec::new_builder()
                .push(bid_data)
                .push(assets_data)
                .build();


            let deps = make_deps(orig_tx.raw().cell_deps(),
                                 last_tx.raw().cell_deps(),
                                 inputs.clone());

            let tx = Transaction::new_builder()
                .raw(RawTransaction::new_builder()
                     .inputs(inputs)
                     .outputs(outputs)
                     .outputs_data(outputs_data)
                     .header_deps(orig_tx.raw().header_deps())
                     .cell_deps(deps)
                     .build())
                 .witnesses(orig_tx.witnesses())
                 .build();

            Ok(tx)
        },
        Request::Open => {
            panic!("Can't rebase opening an auction.");
        },
    }
}

/// Pessmistically merge all of the cell deps we need.
///
/// Right now we just add all of the deps for both parent transactions,
/// as well as our inputs.
///
/// TODO/FIXME: be smarter about this. In particular:
///
/// - Deduplicate deps that appear in multiple places.
/// - Avoid including unnecessary deps. For example, if `last_tx` refunded
///   a bid, it will depend on the refund lock script for that bid -- but
///   we don't care about it because that cell is gone now.
///
/// The latter is actually a non-trivial problem; if this same logic is used
/// across many successive bids, *all* previous bids will always be included
/// as a dependency, so this can cause a space leak.
fn make_deps(orig_deps: CellDepVec,
             last_deps: CellDepVec,
             inputs: CellInputVec) -> CellDepVec {
    let mut deps = orig_deps.as_builder();
    for dep in last_deps {
        deps = deps.push(dep);
    }
    for input in inputs {
        deps = deps.push(CellDep::new_builder()
                         .out_point(input.previous_output())
                         .build());
    }
    deps.build()
}
