//! This script is just a no-op, which always permits transactions.
//!
//! It is useful when we expect the type script to take care of everything,
//! as is the case with the auction state cell.

use core::result::Result;
use avoum_script_common::error::Error;

pub fn main() -> Result<(), Error> {
    Ok(())
}
