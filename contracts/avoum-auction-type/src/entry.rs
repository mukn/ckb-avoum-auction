//! Type script for the auction state.
//!
//! This is where most of the validation logic is; the other scripts in
//! this repository are somewhat-trivial lock scripts.
use core::result::Result;

use ckb_std::{
    high_level,
    high_level::{
        load_cell_data,
        load_header,
    },
    ckb_constants::Source,
    ckb_types::prelude::*,
};

use auction_utils;
use auction_utils::{
    types,

};
use auction_utils::types::Request;

use avoum_script_common::{
    load_request,
    load_auction_state,
    load_type_script,
    error::Error,
};

type CellAddr = (usize, Source);

/// Measure the value of a cell, interpreted as a bid. Panics if the cell is not a valid
/// bid at all.
fn cell_bid_value(cell: CellAddr) -> u64 {
    // Right now this is just the CKB capacity, but we could theoretically do other things,
    // like the value of an SUDT.

    // In addition to reading the value, we also need to make sure there is no type script
    // associated with the cell; otherwise, someone could sabotage the auction by giving us
    // a "bid" whose type rejects future transactions.
    assert!(apply_pair(high_level::load_cell_type, cell)
            .expect("Could not read cell type").is_none());

    apply_pair(high_level::load_cell_capacity, cell).expect("Could not read cell capacity")
}

/// Returns whether the number of inputs/outputs (depending on source) is at most
/// `len`. source should be either Source::Input or Source::Output. TODO: clean up
/// this interface; there must be a more direct way to check the length...
pub fn max_length(len: usize, source: Source) -> bool {
    load_cell_data(len, source).is_err()
}

/// Asserts that the cells at the two positions are equivalent in the following sense:
///
/// - Same type script
/// - Same data contents
/// - Same ckb capacity
///
/// i.e. the lock script/owner may have changed, but otherwise they are identical.
fn assert_cells_equiv((xi, xs): (usize, Source), (yi, ys): (usize, Source)) {
    assert!(load_type_script(xi, xs) == load_type_script(yi, ys));
    assert!(high_level::load_cell_capacity(xi, xs).unwrap() ==
            high_level::load_cell_capacity(yi, ys).unwrap());
    assert!(high_level::load_cell_data_hash(xi, xs).unwrap() ==
            high_level::load_cell_data_hash(yi, ys).unwrap());
}

fn assert_has_lock(cell: CellAddr, expected_script: &types::Script) {
    let actual_script = apply_pair(high_level::load_cell_lock, cell)
        .expect("No lock script on cell");
    assert_eq!(types::Script::from(actual_script), *expected_script);
}

fn assert_escrowed(auction_state: &types::AuctionState, cell: CellAddr) {
    assert_has_lock(cell, &auction_state.escrow_lock_script);
}

/// Misc utility function: apply a function that takes two arguments to a 2-tuple.
///
/// This is mainly useful so we can carry around (index, source) as a single value
/// and then ergonomically pass it to functions that expect these as separate arguments.
fn apply_pair<A, B, C>(f: impl Fn(A, B) -> C, (a, b): (A, B)) -> C {
    f(a, b)
}

/// Asserts that the specified cell shares its type script with the caller,
/// i.e. it is the auction state.
fn assert_auction_type(addr: CellAddr) {
    assert!(apply_pair(load_type_script, addr).expect("Missing type script")
            == types::Script::from(high_level::load_script()
                                   .expect("Failed to load own type script")));
}


pub fn main() -> Result<(), Error> {
    let request = load_request()?;
    match request {
        Request::Open => {
            // Enforce expected numbers of inputs/outputs.
            assert!(max_length(1, Source::Input));
            assert!(max_length(2, Source::Output));

            // Make sure the auction is in an initial state; no bid, all lock scripts
            // point to seller:
            let auction_state = load_auction_state(0, Source::Output)?;
            assert!(auction_state.current_bid == 0);
            assert!(auction_state.refund_lock_script == auction_state.seller_lock_script);

            // Make sure the auction ID is what we expect:
            let input0_out = high_level::load_input_out_point(0, Source::Input)?;
            let expected_avoum_id = types::AvoumId::new(&input0_out, 0);
            assert!(auction_state.avoum_id == expected_avoum_id);
            // Make sure the output is us:
            assert_auction_type((0, Source::Output));

            let assets_output = (1, Source::Output);
            let assets_input = (0, Source::Input);

            // Make sure we own the escrowed assets:
            assert_escrowed(&auction_state, assets_output);

            // Make sure that (but for ownership) the escrowed cell is the same as the input
            // assets:
            assert_cells_equiv(assets_input, assets_output);

            Ok(())
        },
        Request::Bid { refund_lock_script, .. } => {
            assert!(max_length(4, Source::Input));

            let auction_state_input = (0, Source::Input);
            let auction_state_output = (0, Source::Output);
            let assets_input = (1, Source::Input);
            let assets_output = (1, Source::Output);
            let new_bid_input = (2, Source::Input);
            let new_bid_output = (2, Source::Output);
            let old_bid_input = (3, Source::Input);
            let old_bid_output = (3, Source::Output);

            assert_auction_type(auction_state_input);
            assert_auction_type(auction_state_output);

            let auction_in = apply_pair(load_auction_state, auction_state_input)?;
            let auction_out = apply_pair(load_auction_state, auction_state_output)?;

            if auction_in.current_bid == 0 {
                // first bid.
                assert!(max_length(3, Source::Output));
            } else {
                // There should be a bid to refund. make sure we're doing so
                // correctly.
                assert!(max_length(4, Source::Output));
                assert_cells_equiv(old_bid_input, old_bid_output);
                assert_has_lock(old_bid_output, &auction_in.refund_lock_script);
            }

            // Make sure the bid is valid:
            assert!(cell_bid_value(new_bid_output) == auction_out.current_bid);
            assert!(auction_in.current_bid < auction_out.current_bid);
            assert_cells_equiv(new_bid_input, new_bid_output);
            assert_escrowed(&auction_out, new_bid_output);
            assert!(auction_out.refund_lock_script == refund_lock_script);

            // Make sure we're not messing with the assets:
            assert_cells_equiv(assets_input, assets_output);
            assert_escrowed(&auction_in, assets_input);
            assert_escrowed(&auction_out, assets_output);

            // Prevent otherwise mucking with the auction state. We do this
            // by cloning the input state and whitelisting specific fields
            // which may be modified (dependent on checks performed above).
            let mut auction_expected = auction_in;
            auction_expected.current_bid = auction_out.current_bid;
            auction_expected.refund_lock_script = refund_lock_script;
            assert!(auction_out == auction_expected);

            // ...and block changes to its capacity as well:
            assert!(apply_pair(high_level::load_cell_capacity, auction_state_input).unwrap() ==
                    apply_pair(high_level::load_cell_capacity, auction_state_output).unwrap());

            Ok(())
        },
        Request::Close => {
            assert!(max_length(3, Source::Input));
            assert!(max_length(2, Source::Output));

            let auction_state_input = (0, Source::Input);
            let assets_input = (1, Source::Input);
            let assets_output = (1, Source::Output);
            let bid_input = (2, Source::Input);
            let bid_output = (0, Source::Output);

            assert_auction_type(auction_state_input);

            let auction_in = apply_pair(load_auction_state, auction_state_input)?;

            // Ensure the deadline is in the past. TODO: IIRC, there's a bound
            // on how recent a block can be before transactions can reference
            // its headers, so we may want account for that difference and
            // allow blocks that are slightly older than the deadline block,
            // but still young enough that they couldn't be included if the
            // deadline hadn't passed. Need to dig into the details.
            let header_block_number = load_header(0, Source::HeaderDep)?
                .as_reader()
                .raw()
                .number()
                .unpack();
            assert!(auction_in.deadline_block < header_block_number);

            // Validate the funds being sent to the seller:
            assert_cells_equiv(bid_input, bid_output);
            assert_has_lock(bid_output, &auction_in.seller_lock_script);

            // Validate the assets being sent to the highest bidder:
            assert_cells_equiv(assets_input, assets_output);
            assert_has_lock(assets_output, &auction_in.refund_lock_script);

            Ok(())
        },
    }
}
