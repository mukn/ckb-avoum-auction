
use crate::types;
use serde::{Serialize, Deserialize};
use ckb_sodium;

/// Data used by the `avoum-auction-bid-lock` script, which
/// is used to guard pending bids.
///
/// This is stored in a witness, and its hash is stored
/// in the script's args.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct Data {
    /// Hash of the request witness corresponding to this bid.
    pub request_hash: types::Hash,

    /// The script we expect the auction to use.
    pub auction_script: types::Script,

    /// The unique id for this auction, given the above script:
    pub avoum_id: types::AvoumId,

    /// A public key that can be used to sign transactions;
    /// this allows the bidder to retract a bid, by signing
    /// a different transaction that consumes the bid cell.
    pub pub_key: ckb_sodium::PublicKey,
}
