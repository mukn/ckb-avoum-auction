//! This module defines auction-specific types that appear on-chain.
//!
//! We currently use serde for serialization, mainly for expediency,
//! but in the long run it might be nice to switch to molecule, which
//! is used by CKB itself.

use alloc::vec::Vec;
use core::convert::TryInto;
use serde::{Serialize, Deserialize};
use ckb_standalone_types::{
    packed,
    prelude::{Entity, Pack, Unpack, Builder}
};
use ckb_sodium;

pub mod bid_lock;

// TODO(cleanup): it might be cleaner (and faster) to use blake2b as CKB itself does in
// places. But this isn't critical.
use sha2::{Digest, Sha256};

#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
/// A sha256 hash.
pub struct Hash {
    pub digest: [u8; 32],
}

impl Hash {
    pub fn from_data(data: &[u8]) -> Self {
        Hash { digest: Sha256::digest(data).into() }
    }
}

impl From<Sha256> for Hash {
    fn from(h: Sha256) -> Self {
        Hash {
            digest: h.finalize().try_into().expect("Incorrect length"),
        }
    }
}

impl From<packed::Byte32> for Hash {
    fn from(b: packed::Byte32) -> Self {
        Hash {
            digest: b.as_reader().raw_data().try_into().expect("Incorrect length"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Ord, PartialOrd)]
/// CKB's stript data structure. TODO: it is convienent to have a version of
/// this that is parsed into a struct, but in the long run it might be better to
/// leave it in its molecule encoded form.
pub struct Script {
    pub code_hash: Hash,
    pub args: Vec<u8>,
    pub hash_type: u8,
}

impl From<packed::Script> for Script {
    fn from(s: packed::Script) -> Self {
        Script {
            code_hash: s.code_hash().into(),
            hash_type: s.hash_type().into(),
            args: s.args().unpack(),
        }
    }
}

impl From<Script> for packed::Script {
    fn from(s: Script) -> Self {
        packed::Script::new_builder()
            .code_hash(s.code_hash.digest.pack())
            .hash_type(s.hash_type.into())
            .args(s.args.pack())
            .build()
    }
}

/// Unique identifier for use by AVOUM-aware software, such as
/// rebase scripts and indexing miners.
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Eq)]
pub struct AvoumId {
    /// Hash of:
    ///
    /// - The first input to the transaction that created the cell.
    /// - ...and the index of the created cell in the outputs.
    ///
    /// Which, combined with the type script, ensures uniqueness
    pub unique_hash: Hash,
}

impl AvoumId {
    /// Generate a new id, based on an existing OutPoint. This is useful to
    /// ensure uniqueness.
    pub fn new(input: &packed::OutPoint, index: u32) -> Self {
        let mut h = Sha256::new();

        // The exact combination of these is somewhat arbitrary; we just need to include
        // all information in a deterministic way:
        h.update(input.tx_hash().as_reader().raw_data());
        h.update(input.index().as_reader().raw_data());
        h.update(index.to_le_bytes());
        AvoumId { unique_hash: Hash::from(h) }
    }

    /// Combine this with a script to return a unique hash of
    /// (type script, identity). This pair is a "primary key"
    /// used for indexing the identity by rebasing miners.
    ///
    /// Miners are free to store the two separately or combine
    /// them however they please, but it is useful for script
    /// logic to be able to carry them as a single hash.
    pub fn hash_with_type(&self, type_script: &packed::Script) -> Hash {
        let mut h = Sha256::new();
        h.update(type_script.code_hash().as_reader().raw_data());
        h.update(type_script.hash_type().as_slice());
        h.update(type_script.args().as_reader().raw_data());
        h.update(&self.unique_hash.digest[..]);
        Hash::from(h)
    }
}

/// The current state stored in the cell for the auction consensus.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct AuctionState {
    /// Unique ID for this auction.
    ///
    /// TODO: rather than storing this inside the serialized state for
    /// the auction, we should have this just be the first data in the
    /// cell, so that e.g. miners don't need to know about the details
    /// of this particular contract for indexing purposes.
    pub avoum_id: AvoumId,

    /// Value of the current bid.
    pub current_bid: u64,

    /// The block after which the auction is considered to have ended.
    /// To distribute the assets/bid, the transaction must prove that
    /// this is in the past.
    pub deadline_block: u64,

    /// The lock script to attach to the accepted bid, when the
    /// auction is closed; this allows the seller to access it.
    pub seller_lock_script: Script,

    /// The lock script that we expect escrowed cells to have.
    pub escrow_lock_script: Script,

    /// The lock script we should assign to cells sent to the current bidder.
    /// This could be either refunded bids, in the case that we receive
    /// a higher bid, or the purchased assets, should we accept the current
    /// bid as final.
    pub refund_lock_script: Script,
}

/// A `Request` is included as a witness in the transaction, and describes
/// the operation the transaction performs, including any information necessary
/// to verify it beyond the transaction itself.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum Request {
    /// Open the auction in the first place.
    Open,

    /// Place a new bid in the auction.
    Bid {
        /// Lock script to attach to this bid when refunding it, if someone
        /// outbids us.
        refund_lock_script: Script,

        /// Public key that can be used to sign transactions "retracting"
        /// this bid (i.e. transactions that consume the cell without
        /// interacting with the auction).
        retract_key: ckb_sodium::PublicKey,
    },

    /// Close the auction and distribute funds.
    Close,
}
