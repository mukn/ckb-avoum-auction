use core::result::Result;
use core::convert::TryInto;
use crate::load_witness_vec;
use ckb_std::{
    ckb_constants::Source,
    high_level,
};

use ckb_sodium::{
    PublicKey, Signature,
    verify_detached,
};

/// Assert that the transaction is signed by the specified public key.
///
/// The signature must be found in a witness. it may be at any index;
/// this function will search all of them until it either finds a valid
/// signature or runs out of candidates.
pub fn assert_tx_signed_by(pk: &PublicKey) {
    let tx_hash = high_level::load_tx_hash().unwrap();
    for i in 0.. {
        let sig_bytes = load_witness_vec(i, Source::Input).unwrap();
        let arr : Result<[u8; 64], _> = sig_bytes.try_into();
        match arr {
            Err(_) => {}
            Ok(array) => {
                let sig = Signature::from(array);
                if verify_detached(&sig, &tx_hash[..], &pk) {
                    return ();
                }
            }
        }
    }
    panic!("No valid signature found")
}
