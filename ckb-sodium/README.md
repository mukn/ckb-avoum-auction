This crate is a binding to `libsodium`.

Ideally, we would just use the popular `sodiumoxide`, crate, but I was
having trouble getting it to build for CKB, and I had similar problems
with other crypto libraries.

Notes about this crate:

- It includes the C source code directly (via a git submodule) rather
  than pulling in libsodium-sys or trying to find the library on the
  system.
- It currently bypasses libsodium's own build system and just feeds
  most source files to the compiler directly. If we keep this, we should
  try instead to build it with autotools, since the current approach
  is not tested by the `libsodium` developers (indeed, the build
  generates a warning about this). But ideally we would obviate this by
  figuring out why `sodiumoxide` isn't working.
- It is marked `no_std`, but this is a bit messy; it exposes symbols
  that are not actually usable in `no_std`, e.g. `gen_keypair`. Because
  of the way the build works, attempts to use these in a `no_std`
  environment would show up as linker errors. But we only use those
  from the test suite. This is a bit janky, it would be nice to clean
  it up.
