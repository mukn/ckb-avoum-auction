This crate contains the core logic for the auction's rebase script.

Since the exact execution environment is yet to be fully defined, this
provides only a library function, not a full executable that can be run
somewhere.
