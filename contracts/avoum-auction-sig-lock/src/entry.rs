//! Dead simple lock script that just checks for a signature.
//!
//! Expects a libsodium public key in args, and expects a signature of
//! the tranasaction hash in any witness.
//!
//! The test suite uses this as a lock script for cells which are leaving the
//! auction's control, e.g. the refund lock script or the seller's lock script.
//!
//! Likely real users would use one of the existing off the shelf CKB
//! signature lock scripts, which are presumably better audited and more
//! featureful, but this is useful for testing.
use core::result::Result;
use core::convert::TryInto;
use avoum_script_common::{
    error::Error,
    sign::assert_tx_signed_by,
};
use ckb_std::high_level;
use ckb_sodium::PublicKey;

// This script expects its args to be a public key, and then just looks for
// a witness that is a signature of the tx hash.
pub fn main() -> Result<(), Error> {
    let pk_bytes: [u8; 32] = high_level::load_script()?.args()
        .as_reader().raw_data()
        .try_into().unwrap();
    let pk = PublicKey(pk_bytes);
    assert_tx_signed_by(&pk);
    Ok(())
}

